/**
 * Created by Prith on 8/23/14.
 */
;


(function(window, undefined){
	'use strict';

	//This script is running twice due to a fallback non-async script ref at the bottom of the html.
	//Skip running this, if _bl has already been abstracted away.
	if(window._bl.push !== Array.prototype.push){
		//console.log("window._bl is already an instance of BLService. Don't initialize again")
		return;
	}


	var options = {
		alleventsHandlersWildCard: "*"
	};

	function SocialService(){}
	SocialService.prototype.scrape = function(always){
		var product = {
			"product-title": document.querySelector(".product-title").innerHTML,
			"product-image": document.querySelector(".product-image").getAttribute("src")
		};
		this.share(product,
			function(){
				console.log("* Facebook share successful...");
			},
			function(){
				console.log("* Facebook share failed...");
			},
			function(){
				//this callback will be used in the future to trigger TrackingService
				always && always();
			}
		);
	};
	SocialService.prototype.share = function(product, success, error, always){
		console.log("* SocialService: Sharing to Facebook...", product);
		setTimeout(function(){
			//use a random generator as a mock success/error return from facebook api service. make it succeed 80% of the time
			var status = Math.random();
			status = status > 0.8 ? true : false;
			if(status){
				success && success();
			}else{
				error && error();
			}
		},500);
	};

	function FacebookService(){}
	FacebookService.prototype = new SocialService();

	function TrackingService(){}
	TrackingService.prototype.track = function(eventData, eventName){
		console.log("* TrackingService: ", eventName, eventData);
	};

	function BLService(){

		//eventsHandlers contains an object, where
		// keys correspond with the name of an event, and
		// values correspond with an array of all the callbacks / eventhandlers subscribed to that event
		var self = this,
			eventsHandlers = {};

		var publishEventHandlers = function(eventKey, eventName, eventData){
			var currentEventhandlers = eventsHandlers[eventKey];
			if(!currentEventhandlers)
				return;

			for(var i = 0, lenI = currentEventhandlers.length; i<lenI; i++){
				//only call it if the function is defined
				//typeof currentEventhandlers[i] === "function" && currentEventhandlers[i](eventData);
				if(typeof currentEventhandlers[i] === "function"){
					console.log("publishEventHandlers: ", eventName);
					currentEventhandlers[i](eventData, eventName);
				}
			}
		};
		var publishEvent = function(event){
			console.log("publishEvent:", event);
			var eventName = event.name,
				eventData = event.data,
				eventKey;

			//This array contains all the event-handlers that needs to be called
			eventKey = eventName;
			publishEventHandlers(eventKey, eventName, eventData)

			//Also call handlers that are subscribed to all eventsHandlers
			eventKey = options.alleventsHandlersWildCard;
			publishEventHandlers(eventKey, eventName, eventData);

		};
		this.push = function(eventName, eventData){
			publishEvent({name:eventName, data: eventData});
		};
		var publishEventsPreInit = function(){
			var _bl =  window._bl;
			//Publish all the eventsHandlers in the _bl array
			console.log("publishEventsPreInit:", "_bl array events: ", _bl);
			for(var i= 0, lenI=_bl.length; i<lenI; i++){
				self.push(_bl[i]);
			}
		};

		
		var subscribeToEvent = function(eventNameSubscribedTo, callback){
			//Convention: if eventName is undefined or an asterics, subscribe the callback to all eventsHandlers;
			eventNameSubscribedTo = eventNameSubscribedTo || options.alleventsHandlersWildCard;

			eventsHandlers[eventNameSubscribedTo] = eventsHandlers[eventNameSubscribedTo] || [];

			//Add a callback into this array. In publish event, all these callbacks will be executed
			eventsHandlers[eventNameSubscribedTo].push(callback);

			console.log("subscribeToEvent:", eventNameSubscribedTo)
		};

		var initBlService = function(){
			//Replace _bl with this service in order to abstract away .push()
			window._bl = self;
		};
		var init = function(){
			//subscribe to eventsHandlers first, so that none of the eventsHandlers are missed
			var facebookService  = new FacebookService();
			subscribeToEvent('facebook_share',function(){
				facebookService.scrape();
			});
			var trackingService = new TrackingService();
			subscribeToEvent('', trackingService.track);

			publishEventsPreInit();
			initBlService();

		};

		init();
	}
	var blService = new BLService();
	if(options.debug){
		window.blService = blService;
		console.log(blService);
	}
})(window);

